a) This folder includes all output files (*.csv) generated from the MATLAB code.

b) Input files (*.edf) are on the Inception server, at the following path:
	/home/inception/Documents/InceptionSignals 
	
c) The PatientToEncodingLink.xlsx file 
	- shows the correlation between pacients names, input and output files namings
	- columns "Name" and "Input" point to the same file 
		(just naming is adapted so that it corresponds to the convention used in the matlab script: COPDstage_test_IDX.edf) 
	
d) printSignalLables.py script (python2.7) from /home/inception/Documents 
	 - prints only the *.edf files that contain all the requested signals
	 